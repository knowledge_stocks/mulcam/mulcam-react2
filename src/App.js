import { useState } from "react";
import UseEffectEx from "./components/UseEffectEx";
import UseReducerEx from "./components/UseReducerEx";
import UseReducerEx2 from "./components/UseReducerEx2";
import UseMemoEx from "./components/UseMemoEx";
import UseMemoEx2 from "./components/UseMemoEx2";
import UseCallbackEx from "./components/UseCallbackEx";

function App() {
  const [visibility, setVisibility] = useState(true);
  const [mount, setMount] = useState(true);

  return (
    <div>
      <UseCallbackEx/>
      <hr/>
      <UseMemoEx2/>
      <hr/>
      <UseMemoEx/>
      <hr/>
      <UseReducerEx2/>
      <hr/>
      <UseReducerEx/>
      <hr/>
      <p style={{ whiteSpace: 'pre-line' }} >
        {`// display를 none으로 설정하는 것으로는 unmount되지 않는다.
        // 하지만 자식 컴포넌트의 인스턴스는 유지되기 때문에 다시 나타나도 입력된 값이 유지된다.`}
      </p>
      <div style={visibility ? {} : { display: 'none' }}>
        <UseEffectEx/>
      </div>
      <button onClick={() => {setVisibility(!visibility)}}>visibility to {visibility ? 'false' : 'true'}</button>
      <hr/>
      <p style={{ whiteSpace: 'pre-line' }} >
        {`// &&나 삼항연산자로 컴포넌트를 숨길 경우 mount, unmount 이벤트가 발생한다.
        // 하지만 자식 컴포넌트의 인스턴스는 유지되지 않기 때문에 이전에 입력한 값이 초기화된다.`}
      </p>
      {mount && <UseEffectEx/>}
      <button onClick={() => {setMount(!mount)}}>mount to {visibility ? 'false' : 'true'}</button>
    </div>
  );
}

// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;
