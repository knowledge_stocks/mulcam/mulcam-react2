
/*
useCallback은
*/

import { useCallback, useMemo, useState } from "react";

const getAverage = function(nums) {
    const sum = nums.reduce((acc, num) => acc + num, 0);
    if(sum == 0) {
        return 0;
    }
    return sum / nums.length;
}

const UseCallbackEx = function() {
    const [list, setList] = useState([]);
    const [num, setNum] = useState('');

    // list가 변경될 때마다 정의한 함수를 실행한 결과를 저장한다.
    // 이건 forceUpdate로 하는 꼼수를 못 쓰겠는데.. list의 주소가 그대로니까..
    const avg = useMemo(() => {
        const parsedNum = parseInt(num);
        if(!isNaN(parsedNum)) {
            return getAverage([...list, parsedNum])
        } else {
            return getAverage(list)
        }
    }, [list, num]);

    // 이런식으로 이벤트 함수를 선언해놓을 경우
    // 화면이 렌더링될 때마다 함수가 생성된다고 한다.
    // const onChange = function(e) {
    //     setNum(e.target.value);
    // };

    // 컴포넌트가 마운트될 때만 함수가 생성한다.
    // e는 어차피 함수로 들어오는 인자이기 때문에
    const onChange = useCallback(
        function(e) {
            setNum(e.target.value);
        }, []
    );

    // 함수 내부에서 참조하는 값이 변할 수 있기 때문에
    // 내부에서 참조하는 list와 num이 변했을 때 재생성한다.
    const onInsert = useCallback(
        function() {
            // list.push(parseInt(num)); // memo가 적용이 안되기 때문에 못 씀 이건
            const newList = list.concat(parseInt(num));
            setList(newList);
        }, [num, list] // num, list가 변했을 때 함수가 생성된다.
    );

    // 내부에서 참조하는 list가 변했을 때 재생성한다.
    const removeNum = useCallback(
        function(index) {
            list.splice(index, 1);
            setList([...list])
        }, [list]
    );

    return (
        <div>
            <input value={num} onChange={onChange} />
            <button onClick={onInsert}>등록</button>
            <ul>
                {list.map((v,i) => <li onDoubleClick={() => removeNum(i)} key={i}>{v}</li>)}
            </ul>
            <div>
                <b>평균값: </b>{avg}
            </div>
        </div>
    );
};

export default UseCallbackEx;