
/*
useMemo는 컴포넌트 태부에서 발생하는 연산을 최적화할 수 있다.
*/

import { useState } from "react";

const getAverage = function(nums) {
    const sum = nums.reduce((acc, num) => acc + num, 0);
    if(sum == 0) {
        return 0;
    }
    return sum / nums.length;
}

const UseMemoEx = function() {
    const forceUpdate = useState()[1].bind(null, {});

    const [list, setList] = useState([]);
    const [num, setNum] = useState('');

    const onChange = function(e) {
        setNum(e.target.value);
    };

    const onInsert = function() {
        list.push(parseInt(num));
        forceUpdate();
    }

    const removeNum = function(index) {
        list.splice(index, 1);
        forceUpdate();
    }

    return (
        <div>
            <input value={num} onChange={onChange} />
            <button onClick={onInsert}>등록</button>
            <ul>
                {list.map((v,i) => <li onDoubleClick={() => removeNum(i)} key={i}>{v}</li>)}
            </ul>
            <div>
                <b>평균값: </b>{getAverage(list)}
            </div>
        </div>
    );
};

export default UseMemoEx;