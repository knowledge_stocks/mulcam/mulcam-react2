import { useState, useReducer } from "react";

// useReducer: useState보다 더 다양한 컴포넌트 상황에 따라
// 다양한 상태를 다른 값으로 업데이트 해주고 싶을 때 사용한다.
// 리듀서는 현재상태, 그리고 업데이트를 위해 필요한 정보를 담는 액션(action)값을
// 전달받아 새로운 상태를 반환하는 함수이다.
const UseReducerEx = function() {
    function reducer(state, action) {
        switch(action.type) {
            case 'INC':
                return {value: state.value + 1};
            case 'DEC':
                return {value: state.value - 1};
            default:
                return state;
        }
    }

    // useState는 내부적으로 useReducer를 wrapping한다고 한다.
    // useReducer(<action을 받을 함수>, <기본값>);
    const [state, dispatch] = useReducer(reducer, {value: 0});

    return (
        <div>
            <p>
                현재 카운터 값은 <b>{state.value}</b>입니다.
            </p>
            <button onClick={() => dispatch({type: 'INC'})}>+1</button>
            <button onClick={() => dispatch({type: 'DEC'})}>+1</button>
        </div>
    );
};

export default UseReducerEx;