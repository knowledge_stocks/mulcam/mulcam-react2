import { useState, useReducer } from "react";

const reducer = function(state, action) {
    // action은 이벤트를 일으킨 타겟임
    // action.dataset을 통해 data- attribute에 접근할 수 있다.
    return (
        {...state, [action.dataset.name]:action.value}
    );
}

// useReducer: useState보다 더 다양한 컴포넌트 상황에 따라
// 다양한 상태를 다른 값으로 업데이트 해주고 싶을 때 사용한다.
// 리듀서는 현재상태, 그리고 업데이트를 위해 필요한 정보를 담는 액션(action)값을
// 전달받아 새로운 상태를 반환하는 함수이다.
const UseReducerEx2 = function() {
    const [state, dispatch] = useReducer(reducer, {name: '', nickName: ''});
    const {name, nickName} = state; // 요런것도 되네

    const onChange = function(e) {
        dispatch(e.target);
    };

    return (
        <div>
            <div>
                <input data-name="name" value={name} onChange={onChange} />
                <input data-name="nickName" value={nickName} onChange={onChange} />
            </div>
            <div>
                <b>이름: </b>{state.name}
            </div>
            <div>
                <b>닉네임: </b>{state.nickName}
            </div>
        </div>
    );
};

export default UseReducerEx2;