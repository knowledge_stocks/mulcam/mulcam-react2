import { useEffect } from "react";
import { useState } from "react";

const UseEffectEx = function() {
    const [name, setName] = useState('홍길동');
    const [value, setValue] = useState('기본값');
    const [notUsingValue, setNotUsingValue] = useState(); // 사용되지 않는 값

    // useEffect는 컴포넌트가 mount, unmount, update 됐을 때 수행된다.
    // useEffect(() => {
    //     console.log('렌더링 되었습니다.');
    //     console.log({name, value});
    // });

    // 두번째 인자값은 dependencyList가 들어간다.
    // dependencyList는 특정값이 업데이트 될 때 실행하고 싶을 때 지정한다.
    // 컴포넌트가 처음 렌더링될 때 한번만 실행하고 싶다면 빈 배열[]을 넣는다.
    useEffect(() => {
        console.log('컴포넌트가 처음 렌더링(마운트) 되었습니다.');

        // 언마운트 될 때 실행하고 싶은 내용은 return에 지정한다.
        return () => console.log('컴포넌트가 언마운트 되었습니다.');
    }, []);

    useEffect(() => {
        console.log('value가 변경되었습니다.');

        return () => console.log('value쪽에 설정한 unmount 함수. 값이 변경될 때도 호출된다.');
    }, [value]);

    useEffect(() => {
        return () => console.log('사용 안되는 값도 변경되면 unmount쪽이 호출된다.');
    }, [notUsingValue]);

    function showName() {
        alert(name);
        console.log(name); // 클라이언트 사이드의 콘솔 = 브라우저의 콘솔에 출력됨
    }
    function showText(event) {
        setValue(event.target.value);
        setNotUsingValue(value);
    }
    return (
        <div>
            <button onClick={showName}>Show name</button>
            <input type="text" onChange={showText} value={value} />
            <p>{value}</p>
        </div>
    );
};

export default UseEffectEx;