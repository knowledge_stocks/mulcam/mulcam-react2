
/*
useMemo는 컴포넌트 내부에서 발생하는 연산을 최적화할 수 있다.
특정 로직에 의해 발생되는 값을 여러곳에서 재사용하기 위해 사용한다.
*/

import { useMemo, useState } from "react";

const getAverage = function(nums) {
    const sum = nums.reduce((acc, num) => acc + num, 0);
    if(sum == 0) {
        return 0;
    }
    return sum / nums.length;
}

const UseMemoEx2 = function() {
    const [list, setList] = useState([]);
    const [num, setNum] = useState('');

    // list가 변경될 때마다 정의한 함수를 실행한 결과를 저장한다.
    // 이건 forceUpdate로 하는 꼼수를 못 쓰겠는데.. list의 주소가 그대로니까..
    const avg = useMemo(() => {
        const parsedNum = parseInt(num);
        if(!isNaN(parsedNum)) {
            return getAverage([...list, parsedNum])
        } else {
            return getAverage(list)
        }
    }, [list, num]);

    const onChange = function(e) {
        setNum(e.target.value);
    };

    const onInsert = function() {
        // list.push(parseInt(num)); // memo가 적용이 안되기 때문에 못 씀 이건
        const newList = list.concat(parseInt(num));
        setList(newList);
    }

    const removeNum = function(index) {
        list.splice(index, 1);
        setList([...list])
    }

    return (
        <div>
            <input value={num} onChange={onChange} />
            <button onClick={onInsert}>등록</button>
            <ul>
                {list.map((v,i) => <li onDoubleClick={() => removeNum(i)} key={i}>{v}</li>)}
            </ul>
            <div>
                <b>평균값: </b>{avg}
            </div>
        </div>
    );
};

export default UseMemoEx2;